<?php

/*
 *
 *  ____            _        _   __  __ _                  __  __ ____
 * |  _ \ ___   ___| | _____| |_|  \/  (_)_ __   ___      |  \/  |  _ \
 * | |_) / _ \ / __| |/ / _ \ __| |\/| | | '_ \ / _ \_____| |\/| | |_) |
 * |  __/ (_) | (__|   <  __/ |_| |  | | | | | |  __/_____| |  | |  __/
 * |_|   \___/ \___|_|\_\___|\__|_|  |_|_|_| |_|\___|     |_|  |_|_|
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author PocketMine Team
 * @link http://www.pocketmine.net/
 *
 *
*/

namespace pocketmine\tile;

use pocketmine\item\Item;
use pocketmine\level\format\FullChunk;
use pocketmine\nbt\tag\ByteTag;
use pocketmine\nbt\tag\Compound;
use pocketmine\nbt\tag\FloatTag;
use pocketmine\nbt\tag\IntTag;
use pocketmine\nbt\tag\StringTag;
use pocketmine\nbt\NBT;

class ItemFrame extends Spawnable {

	public $map_uuid = -1;

	public function __construct(FullChunk $chunk, Compound $nbt){
		if(!isset($nbt->ItemRotation)){
			$nbt->ItemRotation = new ByteTag("ItemRotation", 0);
		}

		if(!isset($nbt->ItemDropChance)){
			$nbt->ItemDropChance = new FloatTag("ItemDropChance", 1.0);
		}

		parent::__construct($chunk, $nbt);
	}

	public function hasItem(){
		return $this->getItem()->getId() !== Item::AIR;
	}

	public function getItem(){
		if(isset($this->namedtag->Item)){
			return NBT::getItemHelper($this->namedtag->Item);
		}else{
			return Item::get(Item::AIR);
		}
	}

	public function setItem(Item $item = null){
		if($item !== null and $item->getId() !== Item::AIR){
			$this->namedtag->Item = NBT::putItemHelper($item);
		}else{
			unset($this->namedtag->Item);
		}
		$this->onChanged();
	}

	public function getItemRotation(){
		return $this->namedtag->ItemRotation->getValue();
	}

	public function setItemRotation(int $rotation){
		$this->namedtag->ItemRotation = new ByteTag("ItemRotation", $rotation);
		$this->onChanged();
	}

	public function getItemDropChance(){
		return $this->namedtag->ItemDropChance->getValue();
	}

	public function setItemDropChance(float $chance){
		$this->namedtag->ItemDropChance = new FloatTag("ItemDropChance", $chance);
		$this->onChanged();
	}

	public function SetMapID(string $mapid){
		$this->map_uuid = $mapid;
		$this->namedtag->Map_UUID = new StringTag("map_uuid", $mapid);
		$this->onChanged();
	}

	public function getMapID(){
		return $this->map_uuid;
	}

	public function getSpawnCompound(){
		$tag = new Compound("", [
			new StringTag("id", Tile::ITEM_FRAME),
			new IntTag("x", (int) $this->x),
			new IntTag("y", (int) $this->y),
			new IntTag("z", (int) $this->z),
			$this->namedtag->ItemDropChance,
			$this->namedtag->ItemRotation,
		]);
		if($this->hasItem()){
			$tag->Item = $this->namedtag->Item;
			if($this->getItem()->getId() === Item::FILLED_MAP){
				if(isset($this->namedtag->Map_UUID)){
					$tag->Map_UUID = $this->namedtag->Map_UUID;
				}
			}
		}

		return $tag;
	}
}